# 6S-V1.1


## a clone of the legacy 6S code (V1.1), as is needed by Py6S and arcsi.

Py6S does not (yet) work with the latest 6S code (V2.1, 2015) which is available from the main 6S site: http://6s.ltdri.org/
The older 2005 version (V1.1), which _is_ compatible with Py6S, is kind of hard to find on the internet. Therefore we opened this repostitory with a clone of the old V1.1 code.

The included makefile has been slightly adapted so it compiles on more modern linux version (e.g. gfortran instead of g77). This is in line with the edits suggested by the Py6S docs on 6S compilation.

### Building
just typing 'make' should suffice to build the sixs binary.

### Installing
There is no make install target yet. However, the generated 'sixs' binary is statically linked, so it can just be copied to any folder in your PATH to make it available system-wide.



More information about 6S in general can be found on the 6S website: http://6s.ltdri.org/.